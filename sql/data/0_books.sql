START TRANSACTION;

-- 0 Auteur
INSERT INTO Auteur(id, nom, prenom) VALUES
( 1, '???', '???');
INSERT INTO Auteur(id, nom, prenom) VALUES
( 2, 'Bay', 'Jean-Philippe');
INSERT INTO Auteur(id, nom, prenom) VALUES
( 3, 'Bishop', 'Matt');
INSERT INTO Auteur(id, nom, prenom) VALUES
( 4, 'Blaess', 'Christophe');
INSERT INTO Auteur(id, nom, prenom) VALUES
( 5, 'Bloch', 'Joshua');
INSERT INTO Auteur(id, nom, prenom) VALUES
( 6, 'Boursin', 'Jean-Louis');
INSERT INTO Auteur(id, nom, prenom) VALUES
( 7, 'Chomette', 'Thomas');
INSERT INTO Auteur(id, nom, prenom) VALUES
( 8, 'Crocy', 'Philippe');
INSERT INTO Auteur(id, nom, prenom) VALUES
( 9, 'de Brawere', 'Erick');
INSERT INTO Auteur(id, nom, prenom) VALUES
(10, 'Debrauwer', 'Laurent');
INSERT INTO Auteur(id, nom, prenom) VALUES
(11, 'Delannoy', 'Claude');
INSERT INTO Auteur(id, nom, prenom) VALUES
(12, 'Ford', 'John');
INSERT INTO Auteur(id, nom, prenom) VALUES
(13, 'Ferguson', 'Niels');
INSERT INTO Auteur(id, nom, prenom) VALUES
(14, 'Géron', 'Aurélien');
INSERT INTO Auteur(id, nom, prenom) VALUES
(15, 'Godoc', 'Eric');
INSERT INTO Auteur(id, nom, prenom) VALUES
(16, 'Guibert', 'Anne');
INSERT INTO Auteur(id, nom, prenom) VALUES
(17, 'Kohno', 'Tadayoshi');
INSERT INTO Auteur(id, nom, prenom) VALUES
(18, 'Loeliger', 'Jon');
INSERT INTO Auteur(id, nom, prenom) VALUES
(19, 'Lucas', 'Michael W.');
INSERT INTO Auteur(id, nom, prenom) VALUES
(20, 'Martin', 'Robert C.');
INSERT INTO Auteur(id, nom, prenom) VALUES
(21, 'McCullough', 'Matthew');
INSERT INTO Auteur(id, nom, prenom) VALUES
(22, 'Pillou', 'Jean-François');
INSERT INTO Auteur(id, nom, prenom) VALUES
(23, 'Schneler', 'Bruce');
INSERT INTO Auteur(id, nom, prenom) VALUES
(24, 'Steinbeck', 'John');
INSERT INTO Auteur(id, nom, prenom) VALUES
(25, 'Strobel', 'Mathias');
INSERT INTO Auteur(id, nom, prenom) VALUES
(26, 'Taylor', 'Allen G.');
INSERT INTO Auteur(id, nom, prenom) VALUES
(27, 'Twabi', 'Fatmé');
INSERT INTO Auteur(id, nom, prenom) VALUES
(28, 'Victor', 'Jean-Christophe');

-- 0 Editeur
INSERT INTO Editeur(id, nom) VALUES
(1, 'Dunod');
INSERT INTO Editeur(id, nom) VALUES
(2, 'Eyrolles');
INSERT INTO Editeur(id, nom) VALUES
(3, 'Arte');
INSERT INTO Editeur(id, nom) VALUES
(4, '20th Century Fox');
INSERT INTO Editeur(id, nom) VALUES
(5, 'Folio');
INSERT INTO Editeur(id, nom) VALUES
(6, 'First');
INSERT INTO Editeur(id, nom) VALUES
(7, 'ENI');
INSERT INTO Editeur(id, nom) VALUES
(8, 'Eveil et Découvertes');
INSERT INTO Editeur(id, nom) VALUES
(9, 'Tec et Doc Lavoisier');
INSERT INTO Editeur(id, nom) VALUES
(10, 'Addison-Wesley Professional');
INSERT INTO Editeur(id, nom) VALUES
(11, 'Wiley');
INSERT INTO Editeur(id, nom) VALUES
(12, 'Addison-Wesley');
INSERT INTO Editeur(id, nom) VALUES
(13, 'No Starch Press');
INSERT INTO Editeur(id, nom) VALUES
(14, 'O''Reilly Media');
INSERT INTO Editeur(id, nom) VALUES
(15, 'Prentice Hall');

-- 0 MotsCles
INSERT INTO MotsCles VALUES
(1, 'informatique');
INSERT INTO MotsCles VALUES
(2, 'sécurité');
INSERT INTO MotsCles VALUES
(3, 'développement');
INSERT INTO MotsCles VALUES
(4, 'mathématiques');
INSERT INTO MotsCles VALUES
(5, 'Linux');
INSERT INTO MotsCles VALUES
(6, 'pour les nuls');
INSERT INTO MotsCles VALUES
(7, 'fiction');
INSERT INTO MotsCles VALUES
(8, 'temps réel');
INSERT INTO MotsCles VALUES
(9, 'Java');
INSERT INTO MotsCles VALUES
(10, 'C++');
INSERT INTO MotsCles VALUES
(11, 'base de données');
INSERT INTO MotsCles VALUES
(12, 'SQL');
INSERT INTO MotsCles VALUES
(13, 'géographie');
INSERT INTO MotsCles VALUES
(14, 'carte');
INSERT INTO MotsCles VALUES
(15, 'script');
INSERT INTO MotsCles VALUES
(16, 'cryptologie');
INSERT INTO MotsCles VALUES
(17, 'paranoïaque');
INSERT INTO MotsCles VALUES
(18, 'versionnement');
INSERT INTO MotsCles VALUES
(19, 'GnuPG');
INSERT INTO MotsCles VALUES
(20, 'Git');
INSERT INTO MotsCles VALUES
(21, 'design pattern');
INSERT INTO MotsCles VALUES
(22, 'conception');
INSERT INTO MotsCles VALUES
(23, 'chanson');
INSERT INTO MotsCles VALUES
(24, 'comptine');
INSERT INTO MotsCles VALUES
(25, 'enfant');
INSERT INTO MotsCles VALUES
(26, 'classe préparatoire');
INSERT INTO MotsCles VALUES
(27, 'adaptation');
INSERT INTO MotsCles VALUES
(28, 'exercices');
INSERT INTO MotsCles VALUES
(29, 'code');
INSERT INTO MotsCles VALUES
(30, 'refactoring');

-- 0 Support
INSERT INTO Support VALUES
(1, 'Livre');
INSERT INTO Support VALUES
(2, 'CD');
INSERT INTO Support VALUES
(3, 'DVD');
INSERT INTO Support VALUES
(4, 'Vidéo');

-- 1 Document
INSERT INTO Document VALUES
(1 , 1, 1, 'EAN978-2100738830', 'Tout sur la sécurité informatique, 4e édition', 'sécurité informatique', TO_DATE('2016', 'YYYY'), '');
INSERT INTO Document VALUES
(2 , 1, 1, 'EAN978-2100073481', 'Pour mieux développer en C++', 'logiciel informatique', TO_DATE('2003', 'YYYY'), '');
INSERT INTO Document VALUES
(3 , 2, 1, 'EAN978-2212140071', 'Programmer en Java, 9e édition', 'logiciel informatique', TO_DATE('2014', 'YYYY'), '');
INSERT INTO Document VALUES
(4 , 2, 1, 'EAN978-2212135794', 'Scripts shell Linux et Unix', 'système informatique', TO_DATE('2012', 'YYYY'), '');
INSERT INTO Document VALUES
(5 , 3, 3, 'ASINB00DM1CGRY'   , 'Le dessous des cartes, coffret vol. 4', 'géographie', TO_DATE('2013', 'YYYY'), '');
INSERT INTO Document VALUES
(6 , 4, 3, 'ASINB0007QRJBA'   , 'Les raisins de la colère', 'fiction', TO_DATE('1940', 'YYYY'), '');
INSERT INTO Document VALUES
(7 , 5, 1, 'EAN978-2070360833', 'Les raisins de la colère', 'fiction', TO_DATE('1939', 'YYYY'), '');
INSERT INTO Document VALUES
(8 , 6, 1, 'EAN978-2754000932', 'Les maths pour les Nuls', 'mathématiques', TO_DATE('2005', 'YYYY'), '');
INSERT INTO Document VALUES
(9 , 6, 1, 'EAN978-2844277268', 'SQL pour les Nuls', 'base de données informatique', TO_DATE('2005', 'YYYY'), '');
INSERT INTO Document VALUES
(10, 7, 1, 'EAN978-2476089693', 'SQL, les fondamentaux du langage', 'base de données informatique', TO_DATE('2014', 'YYYY'), '');
INSERT INTO Document VALUES
(11, 8, 2, 'EAN334-1348374734', '50 plus belles comptines de maternelle', 'chanson pour enfant', TO_DATE('2009', 'YYYY'), '');
INSERT INTO Document VALUES
(12, 9, 1, 'EAN978-2743015176', 'Mathématiques MPSI 1e année', 'mathématiques prépa', TO_DATE('2014', 'YYYY'), '');
INSERT INTO Document VALUES
(13, 2, 1, 'EAN978-2212142082', 'Solutions temps réel sous Linux', 'système informatique', TO_DATE('2015', 'YYYY'), '');
INSERT INTO Document VALUES
(14, 7, 1, 'EAN978-2746080515', 'Design patterns en Java, 3e édition', 'développement informatique', TO_DATE('2013', 'YYYY'), '');
INSERT INTO Document VALUES
(15,10, 1, 'EAN978-0321247445', 'Introduction to computer security, 1st edition', 'sécurité informatique', TO_DATE('2004', 'YYYY'), '');
INSERT INTO Document VALUES
(16,11, 1, 'EAN978-0470474242', 'Cryptographic engineering, 1st edition', 'cryptographie informatique', TO_DATE('2010', 'YYYY'), '');
INSERT INTO Document VALUES
(17,12, 1, 'EAN978-0321356680', 'Effective Java, 2nd edition', 'développement informatique', TO_DATE('2008', 'YYYY'), '');
INSERT INTO Document VALUES
(18,13, 1, 'EAN978-1593270711', 'PGP and GPG : email for practical paranoid', 'cryptographie informatique', TO_DATE('2006', 'YYYY'), '');
INSERT INTO Document VALUES
(19,14, 1, 'EAN978-1449316389', 'Version control with Git, 2nd edition', 'développement informatique', TO_DATE('2012', 'YYYY'), '');
INSERT INTO Document VALUES
(20,15, 1, 'EAN978-0132350884', 'Clean code, 1st version', 'développement informatique', TO_DATE('2008', 'YYYY'), '');


-- 2 Ecrit
INSERT INTO Ecrit VALUES
(1 , 272);
INSERT INTO Ecrit VALUES
(2 , 192);
INSERT INTO Ecrit VALUES
(3 , 916);
INSERT INTO Ecrit VALUES
(4 , 296);
INSERT INTO Ecrit VALUES
(7 , 640);
INSERT INTO Ecrit VALUES
(8 , 432);
INSERT INTO Ecrit VALUES
(9 , 328);
INSERT INTO Ecrit VALUES
(10, 430);
INSERT INTO Ecrit VALUES
(12,1200);
INSERT INTO Ecrit VALUES
(13, 300);
INSERT INTO Ecrit VALUES
(14, 362);
INSERT INTO Ecrit VALUES
(15, 784);
INSERT INTO Ecrit VALUES
(16, 384);
INSERT INTO Ecrit VALUES
(17, 346);
INSERT INTO Ecrit VALUES
(18, 216);
INSERT INTO Ecrit VALUES
(19, 456);
INSERT INTO Ecrit VALUES
(20, 464);

-- 2 NonEcrit
INSERT INTO NonEcrit VALUES
(5 , TO_DATE('14:30', 'HH24:MI'), NULL, 'PAL');
INSERT INTO NonEcrit VALUES
(6 , TO_DATE('02:09', 'HH24:MI'), NULL, 'PAL');
INSERT INTO NonEcrit VALUES
(11, TO_DATE('00:30', 'HH24:MI'), 50, 'WAV');

-- 2* AuteurDocument
INSERT INTO AuteurDocument VALUES
( 1, 22);
INSERT INTO AuteurDocument VALUES
( 1, 2);
INSERT INTO AuteurDocument VALUES
( 2, 14);
INSERT INTO AuteurDocument VALUES
( 2, 27);
INSERT INTO AuteurDocument VALUES
( 3, 11);
INSERT INTO AuteurDocument VALUES
( 4, 4);
INSERT INTO AuteurDocument VALUES
( 5, 28);
INSERT INTO AuteurDocument VALUES
( 5, 16);
INSERT INTO AuteurDocument VALUES
( 5, 25);
INSERT INTO AuteurDocument VALUES
( 6, 12);
INSERT INTO AuteurDocument VALUES
( 7, 24);
INSERT INTO AuteurDocument VALUES
( 8, 6);
INSERT INTO AuteurDocument VALUES
( 9, 26);
INSERT INTO AuteurDocument VALUES
(10, 15);
INSERT INTO AuteurDocument VALUES
(11, 1);
INSERT INTO AuteurDocument VALUES
(12, 7);
INSERT INTO AuteurDocument VALUES
(12, 8);
INSERT INTO AuteurDocument VALUES
(12, 9);
INSERT INTO AuteurDocument VALUES
(13, 4);
INSERT INTO AuteurDocument VALUES
(14, 10);
INSERT INTO AuteurDocument VALUES
(15, 3);
INSERT INTO AuteurDocument VALUES
(16, 13);
INSERT INTO AuteurDocument VALUES
(16, 23);
INSERT INTO AuteurDocument VALUES
(16, 17);
INSERT INTO AuteurDocument VALUES
(17, 5);
INSERT INTO AuteurDocument VALUES
(18, 19);
INSERT INTO AuteurDocument VALUES
(19, 18);
INSERT INTO AuteurDocument VALUES
(19, 21);
INSERT INTO AuteurDocument VALUES
(20, 20);

-- 2* MotsClesDocument
INSERT INTO MotsClesDocument VALUES
(1, 1);
INSERT INTO MotsClesDocument VALUES
(1, 2);
INSERT INTO MotsClesDocument VALUES
(2, 1);
INSERT INTO MotsClesDocument VALUES
(2, 3);
INSERT INTO MotsClesDocument VALUES
(2, 10);
INSERT INTO MotsClesDocument VALUES
(3, 1);
INSERT INTO MotsClesDocument VALUES
(3, 3);
INSERT INTO MotsClesDocument VALUES
(3, 9);
INSERT INTO MotsClesDocument VALUES
(4, 1);
INSERT INTO MotsClesDocument VALUES
(4, 5);
INSERT INTO MotsClesDocument VALUES
(4, 15);
INSERT INTO MotsClesDocument VALUES
(5, 13);
INSERT INTO MotsClesDocument VALUES
(5, 14);
INSERT INTO MotsClesDocument VALUES
(6, 7);
INSERT INTO MotsClesDocument VALUES
(6, 27);
INSERT INTO MotsClesDocument VALUES
(7, 7);
INSERT INTO MotsClesDocument VALUES
(8, 4);
INSERT INTO MotsClesDocument VALUES
(8, 6);
INSERT INTO MotsClesDocument VALUES
(9, 1);
INSERT INTO MotsClesDocument VALUES
(9, 11);
INSERT INTO MotsClesDocument VALUES
(9, 12);
INSERT INTO MotsClesDocument VALUES
(9, 6);
INSERT INTO MotsClesDocument VALUES
(10, 1);
INSERT INTO MotsClesDocument VALUES
(10, 11);
INSERT INTO MotsClesDocument VALUES
(10, 12);
INSERT INTO MotsClesDocument VALUES
(11, 23);
INSERT INTO MotsClesDocument VALUES
(11, 24);
INSERT INTO MotsClesDocument VALUES
(11, 25);
INSERT INTO MotsClesDocument VALUES
(12, 1);
INSERT INTO MotsClesDocument VALUES
(12, 28);
INSERT INTO MotsClesDocument VALUES
(12, 26);
INSERT INTO MotsClesDocument VALUES
(13, 1);
INSERT INTO MotsClesDocument VALUES
(13, 5);
INSERT INTO MotsClesDocument VALUES
(13, 8);
INSERT INTO MotsClesDocument VALUES
(14, 1);
INSERT INTO MotsClesDocument VALUES
(14, 9);
INSERT INTO MotsClesDocument VALUES
(14, 21);
INSERT INTO MotsClesDocument VALUES
(14, 22);
INSERT INTO MotsClesDocument VALUES
(15, 1);
INSERT INTO MotsClesDocument VALUES
(15, 2);
INSERT INTO MotsClesDocument VALUES
(16, 1);
INSERT INTO MotsClesDocument VALUES
(16, 4);
INSERT INTO MotsClesDocument VALUES
(16, 16);
INSERT INTO MotsClesDocument VALUES
(17, 1);
INSERT INTO MotsClesDocument VALUES
(17, 9);
INSERT INTO MotsClesDocument VALUES
(18, 1);
INSERT INTO MotsClesDocument VALUES
(18, 2);
INSERT INTO MotsClesDocument VALUES
(18, 16);
INSERT INTO MotsClesDocument VALUES
(18, 19);
INSERT INTO MotsClesDocument VALUES
(18, 17);
INSERT INTO MotsClesDocument VALUES
(19, 1);
INSERT INTO MotsClesDocument VALUES
(19, 20);
INSERT INTO MotsClesDocument VALUES
(19, 18);
INSERT INTO MotsClesDocument VALUES
(20, 1);
INSERT INTO MotsClesDocument VALUES
(20, 29);
INSERT INTO MotsClesDocument VALUES
(20, 30);

COMMIT;