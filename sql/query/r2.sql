
select d.titre, d.theme 
from Document d inner join Exemplaire ex on
ex.doc_id = d.id and ex.id in(
	select em.exemplaire_Id 
	from Emprunt em 
	where (emprunteur_id in (
		select e.id
		from Emprunteur e 
		where nom = 'Dupont') 
    	and em.dateEmprunt > TO_DATE('15/11/2002', 'DD/MM/YYYY')
		and	em.dateEmprunt < TO_DATE('15/11/2003', 'DD/MM/YYYY')));
		--r2
