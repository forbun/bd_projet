create or replace view tmp17 as
select mc.id as mcid from Document d inner join
	MotsClesDocument mcd on d.id = mcd.doc_Id
	inner join MotsCles mc on mcd.mc_Id = mc.id
	where d.titre='SQL pour les Nuls';

select id from Document
  where id not in (
  select d.id from Document d inner join
	MotsClesDocument mcd on d.id = mcd.doc_Id
	inner join MotsCles mc on mcd.mc_Id = mc.id
	where mc.id in (select mcid from tmp17))
  order by id;--r17


