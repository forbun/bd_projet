create or replace view tmp16 as select e.nom enom,count(ex.id) curemp from
	Document d inner join Exemplaire ex 
		on d.id = ex.doc_Id
	inner join Emprunt em 
		on em.exemplaire_Id = ex.id
	inner join Editeur e on d.editeur_Id = e.id
	group by e.nom
	order by curemp;

select enom from tmp16 where curemp = (select max(curemp) from tmp16); --r16

