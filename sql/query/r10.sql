create or replace view tmp10 as select distinct ed.nom as nom from Editeur ed
	inner join Document d
		on ed.id = d.editeur_id
	where d.theme like '%mathématiques%' 
	or d.theme like '%informatique%';

select nom from Editeur where nom not in (select nom from tmp10); --r10


