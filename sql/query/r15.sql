create or replace view tmp15 as
select a.id aid from Auteur a inner join
  AuteurDocument ad on ad.auteur_id = a.id inner join
	Document d on ad.doc_Id = d.id
	inner join MotsClesDocument mdc on mdc.doc_Id = d.id
	inner join MotsCles mc on mdc.mc_Id = mc.id
	where mc.motcle='informatique';

create or replace view tmp15_2 as
select a.id  a2id from Auteur a inner join
  AuteurDocument ad on ad.auteur_id = a.id inner join
	Document d on ad.doc_Id = d.id
	inner join MotsClesDocument mdc on mdc.doc_Id = d.id
	inner join MotsCles mc on mdc.mc_Id = mc.id
	where mc.motcle='mathématiques';

select nom from Auteur a where(
	a.id in (select aid from tmp15)
	and
	a.id in (select a2id from tmp15_2));--r15


