select emp.nom nom, emp.prenom prenom
	from Document d inner join Support s
		on d.support_Id = s.id
	inner join Exemplaire ex
		on ex.doc_Id = d.id
	inner join Emprunt em
		on ex.id = em.exemplaire_Id
	inner join Emprunteur emp
		on em.emprunteur_Id = emp.id
	inner join CatEmprunteur ce
		on emp.cat_Id = ce.id
	where(s.nom='DVD'
		and em.dateEmprunt >= (add_months(sysdate,-6))
		and ce.nom='Professionel');--r13


