select em.nom nomEmprunteur, em.prenom prenomEmprunteur,
	d.titre, d.theme, a.nom nomAuteur from 
	Emprunteur em inner join Emprunt emp on em.id = emp.emprunteur_id
	inner join Exemplaire ex on emp.exemplaire_id = ex.id 
	inner join Document d on d.id = ex.doc_id 
	inner join AuteurDocument ad on d.id = ad.doc_id 
	inner join Auteur a on a.id = ad.auteur_id
  order by nomEmprunteur, prenomEmprunteur;--r3

