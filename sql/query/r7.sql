select d.id, count(emp.id) total
	from Document d inner join Exemplaire ex on
		d.id = ex.doc_id
	inner join Emprunt emp on
		ex.id = emp.exemplaire_Id
  group by d.id
  order by d.id;--r7

