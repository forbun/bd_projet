-- Depuis la base de quentin (L3IM5)
Grant select on auteur to l3im7;
Grant select on auteurdocument to l3im7;
Grant select on editeur to l3im7;
Grant select on document to l3im7;

-- Depuis la base de ronan (L3IM7)
select a.nom,a.prenom from l3im5.auteur a inner join l3im5.auteurdocument ad on 
	a.id = ad.auteur_id inner join l3im5.document d on
	d.id = ad.doc_id inner join l3im5.editeur e on
	e.id = d.editeur_id where e.nom='Dunod';
