create or replace view tmp8 as select ed.id, ed.nom, count(d.id) total 
	from Editeur ed
	inner join Document d
		on ed.id = d.editeur_Id
	where d.theme like '%mathématiques%'
	or d.theme like '%informatique%'
	group by ed.id, ed.nom;

select nom from tmp8 where total >= 2; --r8


