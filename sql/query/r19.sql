create or replace view tmp17 as
select mc.id as mcid from Document d inner join
	MotsClesDocument mcd on d.id = mcd.doc_Id
	inner join MotsCles mc on mcd.mc_Id = mc.id
	where d.titre='SQL pour les Nuls';

create or replace view tmp19 as select d.id as did, count(mc.id) as cpt
	from Document d inner join
	MotsClesDocument mcd on d.id = mcd.doc_Id
	inner join MotsCles mc on mcd.mc_Id = mc.id
	inner join tmp17 on tmp17.mcid = mc.id
	group by d.id;

select did from tmp19 where cpt = (select count(mcid) from tmp17);--r19
