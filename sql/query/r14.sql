create or replace view tmp14 as
select d.id did, count(ex.id) totalex from
	Document d inner join Exemplaire ex
		on d.id = ex.doc_Id
	group by d.id;

select did from tmp14 where totalex > (select avg(totalex) from tmp14)
  order by did;--r14 (avg(totalex)) = 2.9)

