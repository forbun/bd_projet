create or replace view tmp12 as select d.id did, ex.id exid
	from Document d inner join Exemplaire ex
		on d.id = ex.doc_Id;

select d.id from Document d where d.id not in
	(select did from tmp12 inner join Emprunt e
		on e.exemplaire_Id = tmp12.exid); --r12


