-- tmp20 : nombre de mots clés pour 'SQL pour les Nuls'
create or replace view tmp20 as
select count(*) totalmc from Document d inner join
	MotsClesDocument mcd on d.id = mcd.doc_Id
	inner join MotsCles mc on mcd.mc_Id = mc.id
	where d.titre='SQL pour les Nuls';

-- tmp20_2 : nombre de mots-clés en commun pour chaque couple de document
create or replace view tmp20_2 as
  select mcd.doc_id d1id, mcd2.doc_id d2id, count(*) cpt
	from MotsClesDocument mcd inner join
  MotsClesDocument mcd2 on mcd2.mc_id = mcd.mc_id
	group by mcd.doc_id, mcd2.doc_id;


select d1id id
from tmp20_2
where d1id in (select id from Document where titre = 'SQL pour les Nuls')
          and
          cpt = (select min(totalmc) from tmp20);--r20
