-- 0  Auteur
CREATE TABLE Auteur (
    id              NUMBER(8)       NOT NULL,
    nom             VARCHAR2(32)    NOT NULL,
    prenom          VARCHAR2(32)    ,
    numeroTel       VARCHAR2(20)    ,
    dateNaissance   DATE            ,
    dateDeces       DATE,
    CONSTRAINT AUTpk PRIMARY KEY(id)
);
-- 0  CatEmprunteur
CREATE TABLE CatEmprunteur (
    id          INTEGER         NOT NULL,
    nom         VARCHAR2(32)    NOT NULL UNIQUE,
    maxEmprunt  NUMBER(3)       NOT NULL,
    CONSTRAINT CATpk PRIMARY KEY(id)
);
-- 0  Editeur
CREATE TABLE Editeur (
    id          NUMBER(8)       NOT NULL,
    nom         VARCHAR2(32)    NOT NULL UNIQUE,
    adresse     VARCHAR2(256)   ,
    numeroTel   VARCHAR2(20)    ,
    CONSTRAINT EDTpk PRIMARY KEY(id)
);
-- 0  MotsCles
CREATE TABLE MotsCles (
    id      NUMBER(8)       NOT NULL,
    motcle  VARCHAR2(64)    NOT NULL UNIQUE,
    CONSTRAINT MCLpk PRIMARY KEY(id)
);
-- 0  Support
CREATE TABLE Support (
    id  NUMBER(8)       NOT NULL,
    nom VARCHAR2(64)    NOT NULL UNIQUE,
    CONSTRAINT SUPpk PRIMARY KEY(id)
);
-- 1  Document
CREATE TABLE Document (
    id          NUMBER(8)       NOT NULL,
    editeur_id  NUMBER(8)       NOT NULL,
    support_id  NUMBER(8)       NOT NULL,
    code        VARCHAR2(24)    NOT NULL,
    titre       VARCHAR2(256)   NOT NULL,
    theme       VARCHAR2(64)    ,
    datePub     DATE            ,
    note        VARCHAR2(1024)  ,
    CONSTRAINT DOCpk PRIMARY KEY(id),
    CONSTRAINT DOCfkEDT FOREIGN KEY(editeur_id) REFERENCES Editeur(id),
    CONSTRAINT DOCfkSUP FOREIGN KEY(support_id) REFERENCES Support(id)
);
-- 1  Emprunteur
CREATE TABLE Emprunteur (
    id          NUMBER(8)       NOT NULL,
    cat_id      NUMBER(8)       NOT NULL,
    nom         VARCHAR2(24)    NOT NULL,
    prenom      VARCHAR2(24)    NOT NULL,
    adresse     VARCHAR2(256)   ,
    numeroTel   VARCHAR2(20)    ,
    actif       NUMBER(1)       NOT NULL,
    CONSTRAINT EPRpk PRIMARY KEY(id),
    CONSTRAINT EPRfkCAT FOREIGN KEY(cat_id) REFERENCES CatEmprunteur(id)
);
-- 2  Ecrit
CREATE TABLE Ecrit (
    doc_id      NUMBER(8)   NOT NULL,
    nbrPages    NUMBER(5)   NOT NULL,
    CONSTRAINT ECRpk PRIMARY KEY(doc_id),
    CONSTRAINT ECRfkDOC FOREIGN KEY(doc_id) REFERENCES Document(id)
);
-- 2  Exemplaire
CREATE TABLE Exemplaire (
    id          NUMBER(8)       NOT NULL,
    doc_id      NUMBER(8)       NOT NULL,
    numRayon    VARCHAR2(12)    ,
    empruntable NUMBER(1)       NOT NULL,
    consultable NUMBER(1)       NOT NULL,
    CONSTRAINT EXMpk PRIMARY KEY(id),
    CONSTRAINT EXMfkDOC FOREIGN KEY(doc_id) REFERENCES Document(id),
    CONSTRAINT EXMckStat CHECK(empruntable = 0 OR consultable <> 0)
);
-- 2  NonEcrit
CREATE TABLE NonEcrit (
    doc_id          NUMBER(8)       NOT NULL,
    duree           DATE            ,
    nbrSousTitre    NUMBER(4)       ,
    format          VARCHAR2(20)    ,
    CONSTRAINT NECpk PRIMARY KEY(doc_id),
    CONSTRAINT NECfkDOC FOREIGN KEY(doc_id) REFERENCES Document(id)
);
-- 1* DureeEmprunt
CREATE TABLE DureeEmprunt (
    cat_id          NUMBER(8)   NOT NULL,
    support_id      NUMBER(8)   NOT NULL,
    dureeEmprunt    NUMBER(3)   NOT NULL,
    CONSTRAINT DEMpk PRIMARY KEY(cat_id,support_id),
    CONSTRAINT DEMfkCAT FOREIGN KEY(cat_id) REFERENCES CatEmprunteur(id),
    CONSTRAINT DEMfkSUP FOREIGN KEY(support_id) REFERENCES Support(id)
);
-- 2* AuteurDocument
CREATE TABLE AuteurDocument (
    doc_id      NUMBER(8)   NOT NULL,
    auteur_id   NUMBER(8)   NOT NULL,
    CONSTRAINT ADOun PRIMARY KEY(auteur_id,doc_id),
    CONSTRAINT ADOfkAUT FOREIGN KEY(auteur_id) REFERENCES Auteur(id),
    CONSTRAINT ADOfkDOC FOREIGN KEY(doc_id) REFERENCES Document(id)
);
-- 2* MotsClesDocument
CREATE TABLE MotsClesDocument (
    doc_id  NUMBER(8)   NOT NULL,
    mc_id   NUMBER(8)   NOT NULL,
    CONSTRAINT MCDpk PRIMARY KEY(doc_id,mc_id),
    CONSTRAINT MCDfkDOC FOREIGN KEY(doc_id) REFERENCES Document(id),
    CONSTRAINT MCDfkMCL FOREIGN KEY(mc_id) REFERENCES MotsCles(id)
);
-- 3* Emprunt
CREATE TABLE Emprunt (
    id                  NUMBER(8)   NOT NULL,
    emprunteur_id       NUMBER(8)   NOT NULL,
    exemplaire_id       NUMBER(8)   NOT NULL,
    dateEmprunt         DATE        NOT NULL,
    dateRetourLimite    DATE        NOT NULL,
    dateRetour          DATE        ,
    CONSTRAINT EPTpk PRIMARY KEY(id),
    CONSTRAINT EPTfkEPR FOREIGN KEY(emprunteur_id) REFERENCES Emprunteur(id),
    CONSTRAINT EPTfkEXM FOREIGN KEY(exemplaire_id) REFERENCES Exemplaire(id),
    CONSTRAINT EPTckdate1 CHECK(dateEmprunt < dateRetourLimite),
    CONSTRAINT EPTckdate2 CHECK(dateRetour IS NULL OR dateEmprunt < dateRetour)
);
