CREATE OR REPLACE TRIGGER xor_ecrit
  BEFORE INSERT ON Ecrit
  FOR EACH ROW
DECLARE
  otherId NUMBER(8);
  duplicated_doc EXCEPTION;
BEGIN
  -- Récupère les ids des autres tables
  SELECT count(*) INTO otherId FROM NonEcrit
  WHERE doc_id = :new.doc_id;
  IF(otherId > 0) THEN
    raise duplicated_doc;
  END IF;
EXCEPTION
  WHEN duplicated_doc THEN
    raise_application_error(-20000, 'Document already subclassed !');
END;
/
CREATE OR REPLACE TRIGGER xor_nonecrit
  BEFORE INSERT ON NonEcrit
  FOR EACH ROW
DECLARE
  otherId NUMBER(8);
  duplicated_doc EXCEPTION;
BEGIN
  -- Récupère les ids des autres tables
  SELECT count(*) INTO otherId FROM Ecrit
  WHERE doc_id = :new.doc_id;
  IF(otherId > 0) THEN
    raise duplicated_doc;
  END IF;
EXCEPTION
  WHEN duplicated_doc THEN
    raise_application_error(-20000, 'Document already subclassed !');
END;
/
CREATE OR REPLACE TRIGGER checkEmprunt
  BEFORE INSERT ON Emprunt
  FOR EACH ROW
DECLARE
  already_borrowed EXCEPTION;
  estEmprunte NUMBER(8);

  no_more_borrow EXCEPTION;
  max_borrow NUMBER(8);
  current_borrow NUMBER(8);

  delayed_borrow EXCEPTION;
  nbr_dlBorrow NUMBER(8);
BEGIN
  -- already_borrowed ?
  SELECT count(*) INTO estEmprunte FROM emprunt
  WHERE exemplaire_id = :new.exemplaire_id
        AND
        dateRetour IS NULL;
  IF (estEmprunte > 0) THEN
    raise already_borrowed;
  END IF;
  -- no_more_borrow ?
  SELECT count(*) INTO current_borrow FROM Emprunt
  WHERE emprunteur_id = :new.emprunteur_id
        AND
        dateRetour IS NULL;
  SELECT maxEmprunt INTO max_borrow FROM CatEmprunteur cem
    INNER JOIN Emprunteur emp ON cem.id = emp.cat_id
  WHERE emp.id = :new.emprunteur_id;
  IF(current_borrow >= max_borrow) THEN
    raise no_more_borrow;
  END IF;
  -- delayed_borrow ?
  SELECT count(*) INTO nbr_dlBorrow FROM Emprunt
  WHERE emprunteur_id = :new.emprunteur_id
        AND
        dateRetour IS NULL
        AND
        dateRetourLimite < SYSDATE;
  IF(nbr_dlBorrow > 0) THEN
    raise delayed_borrow;
  END IF;
EXCEPTION
  WHEN already_borrowed THEN
    raise_application_error(-20001, 'Copy already borrowed !');
  WHEN no_more_borrow THEN
    raise_application_error(-20002, 'You have already borrowed enough documents !');
  WHEN delayed_borrow THEN
    raise_application_error(-20003, 'You have an overdue document !');
END;
